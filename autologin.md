
# Instalación y Configuración de AnyDesk

## Descargar he Instalar AnyDesk
* Windows [AnyDesk](https://anydesk.com/es/downloads/windows)
* MacOS [AnyDesk](https://anydesk.com/es/downloads/mac-os)

## Establecer Contraseña para Iniciar Sesión
* Nos dirigimos al Menú Configuración
  <div><img src="img-autologin/anydesk-menu-configuracion.png" alt="Anydesk Menu Configuracion" height="300"></div>

## Author
* Jorge Lagos Álvarez - jlagos@metrocapital.cl
