# Generar Contraseña de Aplicaciones para Office365

## Nos dirigimos a Office365
* [Office365](https://login.live.com/login.srf?wa=wsignin1.0&rpsnv=13&rver=7.3.6963.0&wp=MBI_SSL&wreply=https%3a%2f%2fwww.microsoft.com%2fes-cl%2fmicrosoft-365%2fbusiness%2fcompare-all-microsoft-365-business-products-b%3f%3d%26ef_id%3dCj0KCQiAtICdBhCLARIsALUBFcHBtvSztPoGYfBj4Pu42uN7a2wsQLIO1eocJwC_Q1lB7aOcpVs9beYaAsMmEALw_wcB%3aG%3as%26OCID%3dAIDcmm409lj8ne_SEM_Cj0KCQiAtICdBhCLARIsALUBFcHBtvSztPoGYfBj4Pu42uN7a2wsQLIO1eocJwC_Q1lB7aOcpVs9beYaAsMmEALw_wcB%3aG%3as%26lnkd%3dGoogle_O365SMB_Brand%26gclid%3dCj0KCQiAtICdBhCLARIsALUBFcHBtvSztPoGYfBj4Pu42uN7a2wsQLIO1eocJwC_Q1lB7aOcpVs9beYaAsMmEALw_wcB%26SilentAuth%3d1&lc=13322&id=74335&aadredir=1)

<br>

## Iniciar Sesión
* Debemos ingresar usuarios y contraseña
    <div>
        <img src="img-outlook-contrasena-aplicaciones/login-user.png" height="300">
        <img src="img-outlook-contrasena-aplicaciones/login-password.png" height="300">
    </div>

<br>

## Menu
* Click en Account Manager
* Click en opción My Microsoft Account
    <div>
        <img src="img-outlook-contrasena-aplicaciones/menu-acount-manager.png" height="300">
    </div>
* Click en opción menu Security
    <div>
        <img src="img-outlook-contrasena-aplicaciones/menu-security.png" height="55">
    </div>

<br>

## Seguridad
* Click en Advanced Security Options
    <div>
        <img src="img-outlook-contrasena-aplicaciones/menu-security-options.png" height="400">
    </div>

<br>

## APP Passwords
* Click en Create a New APP Password 
    <div>
        <img src="img-outlook-contrasena-aplicaciones/menu-app-password.png" height="150">
    </div>
* Les volvera a pedir ingresar contraseña
    <div>
        <img src="img-outlook-contrasena-aplicaciones/login-password.png" height="300">
    </div>
* Se ha generado la contraseña de aplicaciones, le damos click al button Done
    <div>
        <img src="img-outlook-contrasena-aplicaciones/app-password-sign-in.png" height="400">
    </div>

<br>

## Author
* Jorge Lagos Álvarez - jlagos@metrocapital.cl