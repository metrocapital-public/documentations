# <h1>Update Chrome Driver</h1>

####  <h2>Validacion Chrome del Equipo</h2>

* Abrir Google Chrome.
    <div><img src="update-chrome-driver/icono-chrome.png" alt="Abrir Google Chrome" height="100"></div>

* Haz clic en el icono de los tres puntos en la esquina superior derecha de la ventana del navegador
    <div><img src="update-chrome-driver/tres_puntos.png" alt="Abrir Menu" height="90"></div>

* En el menú desplegable que aparece, selecciona "Ayuda".
    <div><img src="update-chrome-driver/ayuda.png" alt="Abrir Menu" height="50"></div>

* En el submenú que aparece, selecciona "Información de Google Chrome".
    <div><img src="update-chrome-driver/informacion.png" alt="Abrir Menu" height="20"></div>

* En la nueva pestaña que se abre, encontrarás la versión actual del navegador en la parte superior de la página.
    <div><img src="update-chrome-driver/version.png" alt="Abrir Menu" height="120"></div>


<br>

####  <h2>Validar version de Chrome actual de su equipo</h2>


* Dirigirse a la siguiente URL [Click Aqui](https://chromedriver.chromium.org/downloads) o ```https://chromedriver.chromium.org/downloads```

* Comparar la versión que aparece en la página y la versión de Chrome actual.
    <div><img src="update-chrome-driver/versiones_compara.png" alt="Abrir Menu" height="120"></div>
* En caso de tener una versión diferente, leer la ```Actualización de Chrome Driver```
<br>

#### <h2>Actualizacion de Chome Driver</h2>
* Dirigirse a la siguiente URL ```https://chromedriver.chromium.org/downloads ```
* Descargar la misma versión que tiene el navegador previamente revisado.
    <div><img src="update-chrome-driver/driver.png" alt="Abrir Menu" height="120"></div>
    En este caso, nosotros tenemos la 112.0.5615
* Presionar el enlace el cual los llevará a una nueva pantalla similar a esta:
    <div><img src="update-chrome-driver/index.png" alt="Abrir Menu" height="150"></div>
    
* Elegir el archivo según nuestro sistema operativo```(Window - Linux - Mac)```
* Descargar el archivo en la ubicación predeterminada o elegida por el usuario.
* Descomprimir el archivo en una carpeta. 
* Copiar el archivo descomprimido llamado ```chromedriver```
    <div><img src="update-chrome-driver/chrome_driver.png" alt="Abrir Menu" height="30"></div>
* Dirigirse a la ubicación de instalación de ```Rocketbot``` e ingresar a ```Rocketbot\drivers\win\chrome```.
* Reemplazar el archivo ```chromedriver``` por el descargado.
* Iniciamos ```Rocketbot```.<br>
    <div><img src="update-chrome-driver/rocketbot.png" alt="Abrir Menu" height="50"></div>

 ## Author
    ```
    - Tomás Carvajal, tcarvajal@metrocapital.cl
    ```