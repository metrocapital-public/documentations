# Rocketbot - System Specifications

## OS :computer:

* Windows
  * Windows 7/8/9/10/11 64x
  * Windows Server 2016 R2 o superior 64x

* Linux
  * Ubuntu 18.04 o superior
  * RHEL

* MacOS
  * Sierra
  * Mojave
  * Catalina (Se debe instalar Xcode, además se debe permitir la terminal en ```Seguridad y Privacidad``` si se utiliza otro terminal)

## Requisitos mínimos :wrench:
```js
CPU     Intel Core I3
RAM     +4GB
DISK    100GB
```

## Requisitos recomendados :wrench:
```js
CPU     Intel Core I5 o Superior
RAM     +8GB
DISK    300GB
```

## Softwares :wrench:
* [Rocketbot](https://www.rocketbot.com/rocketbot-studio-rpa/#descarga)
* [Python](https://www.python.org/downloads)
* [Visual Studio Code](https://code.visualstudio.com)
* [HeidiSQL](https://www.heidisql.com/download.php)
* [Google Chrome](https://www.google.com/intl/es/chrome)

## Author
* Jorge Lagos
